import {
    applyMiddleware,
    compose,
    createStore
} from "redux";
import thunk from "redux-thunk";
import {routerMiddleware as router} from 'react-router-redux'
import history from "./history"
import rootReducer from "./reducers";


const configureStore = (initialState = {}) => {

    const thunkMiddleware = thunk;
    const routerMiddleware = router(history);

    const enhancers = [
        applyMiddleware(routerMiddleware, thunkMiddleware)
    ];

    const store = createStore(rootReducer, initialState, compose(...enhancers));

    if (module.hot) {
        module.hot.accept('./reducers', () => {
            const nextReducer = require('./reducers').default;
            store.replaceReducer(nextReducer);
        });
    }

    return store;

};

export default configureStore;