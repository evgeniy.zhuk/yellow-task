import {LOG_IN_SUCCESS, LOG_OUT} from "../actions/SessionActions";

const initialState = {
	isLoggedIn: !!sessionStorage.getItem("jwt_token")
};

const SessionReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOG_IN_SUCCESS:
			return {
				...state,
				isLoggedIn: !!sessionStorage.getItem("jwt_token")
			};

		case LOG_OUT:
			return {
				...state,
				isLoggedIn: !!sessionStorage.getItem("jwt_token")
			};

		default:
			return state;
	}
};


export default SessionReducer;
