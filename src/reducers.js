import {combineReducers} from "redux"
import {routerReducer as router} from 'react-router-redux';
import session from "./reducers/SessionReducer"
import jogs from "./pages/track-list/TrackListReducer"

export default combineReducers({
	jogs,
	session,
	router
})
