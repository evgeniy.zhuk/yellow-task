class JogTrackerService {

	static requestHeaders() {
		return {'AUTHORIZATION': `Bearer ${sessionStorage.getItem("jwt_token")}`}
	}

	static getJogList() {
		const headers = this.requestHeaders();
		const request = new Request('http://jogtracker.herokuapp.com/api/v1/data/sync', {
			method: 'GET',
			headers: headers
		});

		return fetch(request).then(response => {
			return response.json();
		}).catch(error => {
			return error;
		});
	}

	static createJog({distance, time, date}) {

		const headers = this.requestHeaders();

		const request = new Request('http://jogtracker.herokuapp.com/api/v1/data/jog', {
			method: 'POST',
			headers: headers,
			body: JSON.stringify({distance, time, date})
		});

		return fetch(request).then(response => {
			return response.json();
		}).catch(error => {
			return error;
		});

	}

}


export default JogTrackerService;
