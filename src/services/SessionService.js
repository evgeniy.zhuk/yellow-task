class SessionService {

	static login() {
		const uri = "http://jogtracker.herokuapp.com/api/v1/auth/uuidLogin";

		const request = new Request(uri, {
			method: "POST",
			headers: new Headers({
				"Content-Type": "application/json"
			}),
			body: JSON.stringify({uuid: "hello"})
		});

		return fetch(request)
			.then(response => {
				return response.json()
			})
			.catch(err => {
				return err
			})

	}
}


export default SessionService;
