import React from 'react';
import {render} from 'react-dom';
import App from './modules/App/App';
import registerServiceWorker from './registerServiceWorker';

import configureStore from "./store";
import history from "./history";

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const mountApp = document.getElementById('root');
const store = configureStore();



render(
	<App
		store={store}
		history={history}
	/>,
	mountApp
);


registerServiceWorker();
