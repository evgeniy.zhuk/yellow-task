import React from 'react';
import LetMeIn from "./components/let-me-in/LetMeIn";
import {Col, Container, Row} from "reactstrap";

import "./HomePage.css"

const HomePage = () => (
	<main>
		<Container>
			<Row className={"vertical-center"}>
				<Col>
					<LetMeIn/>
				</Col>
			</Row>
		</Container>
	</main>
);


export default HomePage;
