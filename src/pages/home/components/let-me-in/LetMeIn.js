import React, {Component} from 'react';
import {Button, Col, Container, Row} from "reactstrap";
import {connect} from "react-redux";
import {loginUser} from "../../../../actions/SessionActions";

import "./LetMeIn.css";

class LetMeIn extends Component {

	createUser = () => {
		this.props.dispatch(loginUser())
	};

	render() {
		return (
			<Container className={"let-me-in-wrapper"}>
				<Row className={"let-me-in-container"}>
					<Col className={"text-center"}>
						<picture>
							<img src="/bear-face.svg"
									 className="bear-face"/>
						</picture>
					</Col>
				</Row>
				<Row>
					<Col className={"text-center sign-in-wrapper"}>
						<Button
							outline
							className={"sign-in-btn"}
							onClick={this.createUser}
						>Let me in</Button>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default connect(
	state => ({
		router: state.router
	})
)(LetMeIn);
