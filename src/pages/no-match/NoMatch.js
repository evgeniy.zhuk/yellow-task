import React from "react";
import Status from "./components/Status/Status";

import "./NoMatch.css";


const NoMatch = () => (
	<Status code={404}>
		<div className="container-no-match">
			<div className="boo-wrapper">
				<div className="boo">
					<div className="face"/>
				</div>
				<div className="shadow"/>
				<h1>Ooops!</h1>
				<p>
					It seem to we can not
					<br/>
					find page you are looking for
				</p>
			</div>
		</div>
	</Status>
);

export default NoMatch;
