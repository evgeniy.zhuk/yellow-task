import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {createJog} from "../../../track-list/TrackListActions";


import "./TaskCreationForm.css"
import {connect} from "react-redux";

class TaskCreationForm extends Component {

	state = {
		distance: 0,
		time: 0.0,
		date: (new Date()).toLocaleString()
	};

	onSubmit = (evt) => {
		evt.preventDefault();

		evt.persist();

		this.props.dispatch(createJog({
			distance: this.state.distance,
			time: this.state.time,
			date: this.state.date
		}))
	};

	render() {
		return (
			<Form method={"POST"} onSubmit={this.onSubmit}>
				<FormGroup row>
					<Label for="train-distance" sm={2}>Distance</Label>
					<Col sm={10}>
						<Input
							type="text"
							value={this.state.distance}
							onChange={(evt) => {
								this.setState({
									distance: evt.target.value
								})
							}}
							name="train-distance"
							id="train-distance"
						/>
					</Col>
				</FormGroup>
				<FormGroup row>
					<Label for="train-time" sm={2}>Time</Label>
					<Col sm={10}>
						<Input
							type="text"
							name="train-time"
							onChange={(evt) => {
								this.setState({
									time: evt.target.value
								})
							}}
							id="train-time"
						/>
					</Col>
				</FormGroup>
				<FormGroup row>
					<Label for="train-date" sm={2}>Date</Label>
					<Col sm={10}>
						<Input
							type="date"
							name="train-date"
							onChange={(evt) => {
								this.setState({
									date: (new Date(evt.target.value)).toLocaleString()
								})
							}}
							id="train-date"
						/>
					</Col>
				</FormGroup>
				<Row>
					<Col className={"text-center"}>
						<Button
							outline
							className={"create-task-btn"}
							type={"submit"}
						>Save</Button>
					</Col>
				</Row>
			</Form>
		);
	}
}


export default connect(
	state => ({
		router: state.router
	})
)(TaskCreationForm);
