import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Container, Row} from "reactstrap";
import TaskCreationForm from "./components/task-creation-form/TaskCreationForm";

import "./TrainCreationPage.css"

class TrainCreationPage extends Component {
	render() {
		return (
			<main>
				<Container>
					<Row className={"vertical-center"}>
						<Col className={"task-creation-wrapper"}>
							<TaskCreationForm/>
						</Col>
					</Row>
				</Container>
			</main>
		);
	}
}

TrainCreationPage.propTypes = {};

export default TrainCreationPage;
