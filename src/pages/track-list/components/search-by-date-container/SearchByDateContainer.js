import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Container, FormGroup, Input, Label, Row} from "reactstrap";


import "./SearchByDateContainer.css"
import {filterJogList} from "../../TrackListActions";
import {connect} from "react-redux";

class SearchByDateContainer extends Component {

	state = {
		from: null,
		to: null
	};

	onDateChange = (evt) => {
		evt.persist();

		this.setState({
			[evt.target.name]: evt.target.value
		}, () => {
			this.props.dispatch(filterJogList({
				from: this.state.from,
				to: this.state.to
			}))
		})
	};


	render() {
		return (
			<Container fluid className={"search-by-date-container"}>
				<Row>
					<Col className={"col-6"}>
						<FormGroup row>
							<Label for="date-from" sm={{size: 3, offset: 4}} className={"pr-0"}>Date from</Label>
							<Col sm={4}>
								<Input
									className={"filter-date"}
									type="date"
									name="from"
									onChange={this.onDateChange}
									id="date-from"
								/>
							</Col>
						</FormGroup>
					</Col>
					<Col className={"col-6"}>
						<FormGroup row>
							<Label for="date-to" sm={3} className={"pr-0"}>Date to</Label>
							<Col sm={4}>
								<Input
									className={"filter-date"}
									type="date"
									name="to"
									onChange={this.onDateChange}
									id="date-to"
								/>
							</Col>
						</FormGroup>
					</Col>
				</Row>
			</Container>
		);
	}
}


export default connect(
	state => ({
		route: state.route
	})
)(SearchByDateContainer);
