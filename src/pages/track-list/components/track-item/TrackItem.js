import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Row} from "reactstrap";


import "./TrackItem.css"

const TrackItem = (props) => {
	return (
		<Row>
			<Col className={"text-right"}>
				<img src="/runner-icon.svg"
						 className="icon"/>
			</Col>
			<Col>
				<ul className={"train-stats"}>
					<li>
						{(new Date(props.date)).toLocaleDateString()}
					</li>
					<li>
						<strong>Speed:&nbsp;</strong>
						<span>15</span>
					</li>
					<li>
						<strong>Distance:&nbsp;</strong>
						<span>{props.distance} km</span>
					</li>
					<li>
						<strong>Time:&nbsp;</strong>
						<span>{props.time} min</span>
					</li>
				</ul>
			</Col>
		</Row>
	);
};

TrackItem.propTypes = {
	distance: PropTypes.number.isRequired,
	time: PropTypes.number.isRequired,
	date: PropTypes.any.isRequired
};

export default TrackItem;
