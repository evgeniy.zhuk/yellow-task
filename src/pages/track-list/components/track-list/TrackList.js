import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TrackItem from "../track-item/TrackItem";
import {Container} from "reactstrap";

import "./TrackList.css"
import {getUserJogList} from "../../TrackListActions";
import {connect} from "react-redux";



class TrackList extends Component {

	state = {
		count: 6,
		jogList: []
	};


	componentDidMount() {
		this.props.dispatch(getUserJogList())
	}


	componentWillReceiveProps(nextProps) {

		console.log(nextProps)


		this.setState({
			jogList: [...nextProps.jogs.jogList]
		})

	}


	renderJogList = (jog, id) => {
		return (
			<TrackItem
				key={id}
				distance={jog.distance}
				time={jog.time}
				date={jog.date}
			/>
		)
	};


	render() {
		return (
			<Container className={"track-list-container"}>
				{this.state.jogList.map(this.renderJogList)}
			</Container>
		);
	}
}


export default connect(
	state => ({
		jogs: state.jogs
	})
)(TrackList);
