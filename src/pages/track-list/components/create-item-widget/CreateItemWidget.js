import React, {Component} from 'react';
import PropTypes from 'prop-types';

import "./CreateItemWidget.css"
import {connect} from "react-redux";
import {routerActions} from "react-router-redux";

class CreateItemWidget extends Component {


	createTask = () => {
		this.props.dispatch(routerActions.push("train-creation"))
	};

	render() {
		return (
			<button
				title='Create task'
				className='create-task-widget'
				onClick={this.createTask}>
				<img src="/add-task-icon.svg"
						 className="add"/>
			</button>
		);
	}
}


export default connect(
	state => ({
		router: state.router
	})
)(CreateItemWidget);
