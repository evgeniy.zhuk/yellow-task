import {ADD_JOG, ADD_TO_JOG_LIST, FILTER_JOG_LIST} from "./TrackListActions";

import moment from "moment"

const initialState = {
	jogList: [],
	filteredJogList: []
};


const TrackListReducer = (state = initialState, action) => {

	switch (action.type) {
		case ADD_TO_JOG_LIST:

			console.log(action.payload)

			return {
				...state,
				jogList: state.jogList.concat(action.payload.jogList)
			};

		case ADD_JOG:
			return {
				...state,
				jogList: [action.payload.jog, ...state.jogList]
			};

		case FILTER_JOG_LIST:

			return {
				...state,
				jogList: [...state.jogList
					.filter(jog => {

						if (action.payload.from && !action.payload.to) {
							return moment(jog.date).isAfter(action.payload.from)
						} else if (action.payload.to && !action.payload.from) {
							return moment(jog.date).isBefore(action.payload.to)
						} else if (action.payload.from && action.payload.to) {
							return moment(jog.date).isAfter(action.payload.from) && moment(jog.date).isBefore(action.payload.to)
						}
						return jog;
					})]
			};

		default:
			return state;
	}

};


export default TrackListReducer;
