import JogTrackerService from "../../services/JogTrackerService"
import * as routerActions from "react-router-redux";

export const ADD_TO_JOG_LIST = "ADD_TO_JOG_LIST";
export const ADD_JOG = "ADD_JOG";

export const FILTER_JOG_LIST = "FILTER_JOG_LIST";

export const addToJogList = (jogList) => {
	return {
		type: ADD_TO_JOG_LIST,
		payload: {
			jogList
		}
	}
};

export const addJog = (jog) => {
	return {
		type: ADD_JOG,
		payload: {
			jog
		}
	}
};

export const filterJogList = ({from, to}) => {
	return {
		type: FILTER_JOG_LIST,
		payload: {
			from,
			to
		}
	}
};

export const createJog = (jogCredentials) => {
	return (dispatch) => {
		return JogTrackerService.createJog(jogCredentials)
			.then(data => {

				const newJog = {
					distance: data.response.distance,
					time: data.response.time,
					date: +new Date(data.response.date)
				};

				dispatch(addJog(newJog));

				dispatch(routerActions.push("track-list"))

			})
			.catch(err => console.error(err))
	}
};

export const getUserJogList = () => {
	return (dispatch) => {
		return JogTrackerService.getJogList()
			.then(data => data.response)
			.then(response => {
				const userID = response["users"]
					.filter(user => user.email.includes("hello"))[0].id;
				return response["jogs"].filter(jog => jog["user_id"] === userID)
			})
			.then(jogList => {
				dispatch(addToJogList(jogList))
			})
			.catch(err => console.error(err))
	}
};
