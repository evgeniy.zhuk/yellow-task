import React from 'react';
import SearchByDateContainer from "./components/search-by-date-container/SearchByDateContainer";
import TrackList from "./components/track-list/TrackList";
import CreateItemWidget from "./components/create-item-widget/CreateItemWidget";

const TrackListPage = () => {
	return (
		<main>
			<SearchByDateContainer/>
			<TrackList/>
			<CreateItemWidget/>
		</main>
	);
};


export default TrackListPage;
