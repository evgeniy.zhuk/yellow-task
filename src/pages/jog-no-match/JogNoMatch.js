import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Col, Container, Row} from "reactstrap";


import "./JogNoMatchPage.css"

class JogNoMatchPage extends Component {
	render() {
		return (
			<main>
				<Container className={"jog-no-match-wrapper vertical-center"}>
					<Row className={"no-match-icon-wrapper"}>
						<Col>
							<img src="/no-match.svg"
									 className="no-match-icon"/>
						</Col>
					</Row>
					<Row className={"no-match-text"}>
						<Col>Nothing is there</Col>
					</Row>
					<Row>
						<Col>
							<Button
								outline
								className={"create-jog-btn"}
							>Create your jog first</Button>
						</Col>
					</Row>
				</Container>
			</main>
		);
	}
}

JogNoMatchPage.propTypes = {};

export default JogNoMatchPage;
