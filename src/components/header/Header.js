import React, {PureComponent} from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
} from 'reactstrap';
import {connect} from "react-redux";
import Logo from "./components/logo/Logo";

import "./Header.css"

class Header extends PureComponent {

	state = {
		isOpen: false
	};

	toggle = () => {
		this.setState({
			isOpen: !this.state.isOpen
		});
	};

	isHomePage = () => this.props.router.location.pathname === "/";

	render() {
		return (
			<header>
				<Navbar expand="md">
					<NavbarBrand href="/">
						<Logo/>
					</NavbarBrand>
					<NavbarToggler onClick={this.toggle}/>
					{
						!this.isHomePage() &&
						this.props.session.isLoggedIn && (
							<Collapse isOpen={this.state.isOpen} navbar>
								<Nav className="ml-auto" navbar>
									<NavItem>
										<NavLink href="/jobs" className={"is-active"}>JOBS</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="/info">INFO</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="/not-found">CONTACT US</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="/filter">
											<img src="/filter-active.svg"
													 className="filter_active"/>
										</NavLink>
									</NavItem>
								</Nav>
							</Collapse>
						)
					}

				</Navbar>
			</header>
		);
	}
}

export default connect(
	state => ({
		session: state.session,
		router: state.router
	})
)(Header);
