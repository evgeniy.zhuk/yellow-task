import React from 'react';

const Logo = () => (
	<div>
		<img src="/logo.svg"
				 className="logo"/>
	</div>
);


export default Logo;
