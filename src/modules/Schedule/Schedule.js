import React, {Fragment} from "react";
import PropTypes from "prop-types";
import Header from "../../components/header/Header";


const Schedule = ({routes}) => (
	<Fragment>
		<Header/>
		{routes}
	</Fragment>
);

Schedule.propTypes = {
	routes: PropTypes.object.isRequired
};

export default Schedule;
