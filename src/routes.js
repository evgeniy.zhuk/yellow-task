import React from "react";
import {Switch, Route} from "react-router"
import Loadable from "react-loadable";


const LazyHome = Loadable({
	loader: () =>
		import(/* webpackChunkName: "home" */ "./pages/home/HomePage"),
	loading: () => <div>Loading component</div>
});

const LazyTrackList = Loadable({
	loader: () =>
		import(/* webpackChunkName: "track-list" */ "./pages/track-list/TrackListPage"),
	loading: () => <div>Loading component</div>
});

const LazyInfoPage = Loadable({
	loader: () =>
		import(/* webpackChunkName: "info" */ "./pages/info/InfoPage"),
	loading: () => <div>Loading component</div>
});

const LazyTrainCreationPage = Loadable({
	loader: () =>
		import(/* webpackChunkName: "train-creation" */ "./pages/train-creation/TrainCreationPage"),
	loading: () => <div>Loading component</div>
});

const LazyJogNoMatch = Loadable({
	loader: () =>
		import(/* webpackChunkName: "no-match" */ "./pages/jog-no-match/JogNoMatch"),
	loading: () => <div>Loading component</div>
});

const LazyNotFound = Loadable({
	loader: () =>
		import(/* webpackChunkName: "no-match" */ "./pages/no-match/NoMatch"),
	loading: () => <div>Loading component</div>
});


export default (
	<Switch>
		<Route exact path="/" component={LazyHome}/>
		<Route exact path="/track-list" component={LazyTrackList}/>
		<Route exact path="/info" component={LazyInfoPage}/>
		<Route exact path="/train-creation" component={LazyTrainCreationPage}/>
		<Route exact path="/jog-no-match" component={LazyJogNoMatch}/>
		<Route path="*" component={LazyNotFound}/>
	</Switch>
)
