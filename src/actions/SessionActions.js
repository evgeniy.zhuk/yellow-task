import SessionService from "../services/SessionService";
import {routerActions} from "react-router-redux";


export const LOG_IN_SUCCESS = "LOG_IN_SUCCESS";
export const LOG_OUT = 'LOG_OUT';

export function loginSuccess() {
	return {
		type: LOG_IN_SUCCESS
	}
}

export function logOutUser() {
	sessionStorage.removeItem('jwt_token');
	return {type: LOG_OUT}
}


export const goToListPage = () => (
	routerActions.push("track-list")
);

export const loginUser = () => {
	return (dispatch) => {

		return SessionService.login()
			.then(data => data.response)
			.then(response => {
				const token = response["access_token"];

				sessionStorage.setItem("jwt_token", token);
				dispatch(loginSuccess());

				dispatch(routerActions.push("track-list"));
			})
			.catch(err => console.error(err))
	}
};
